#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>

//stocke la représentation binaire du mpz_t n dans le tableau d'entiers n_binaire
void binaire(const mpz_t e, int *e_binaire, int nombreBit)
{
  int i;

  for(i = nombreBit-1; i >= 0 ; i--)
  {
    e_binaire[i] = mpz_tstbit(e, i);
  }
}

//square and multiply, suit l'algorithme numéro 3 de l'énoncé
void expo_mod_rapide(mpz_t a, int *e_binaire, mpz_t n, mpz_t res, int nombreBit)
{
  int i;
  mpz_set(res, a);
  nombreBit = nombreBit-2;
  for(i = nombreBit; i >= 0 ; i--)
  {
    mpz_mul(res, res, res);
    if (e_binaire[i] == 1)
    {
      mpz_mul(res, res, a);
    }
    mpz_mod(res, res, n);
  }
}

//test de fermat
char *test_fermat(mpz_t a, int k)
{
  int i, nombreBit;
  mpz_t b;
  mpz_t max;
  mpz_t exposant;
  mpz_t resultat;
  gmp_randstate_t(alea);

  mpz_init(b);
  mpz_init(resultat);
  mpz_init(exposant);
  mpz_init(max);
  gmp_randinit_default(alea);
  mpz_sub_ui(max, a, 3);
  mpz_sub_ui(exposant, a, 1);

  //calcul de la représentation binaire d'exposant
  nombreBit = mpz_sizeinbase(exposant, 2);
  int exp_binaire[nombreBit];
  binaire(exposant, exp_binaire, nombreBit);

  //quelques valeurs automatiques
  if(mpz_cmp_ui(a, 1) == 0)
  {
    mpz_clear(b);
    mpz_clear(max);
    mpz_clear(resultat);
    gmp_randclear(alea);
    mpz_clear(exposant);
    return "c'est 1";
  }
  if(mpz_cmp_ui(a, 2) == 0)
  {
    mpz_clear(b);
    mpz_clear(max);
    mpz_clear(resultat);
    gmp_randclear(alea);
    mpz_clear(exposant);
    return "premier";
  }
  if(mpz_cmp_ui(a, 3) == 0)
  {
    mpz_clear(b);
    mpz_clear(max);
    mpz_clear(resultat);
    gmp_randclear(alea);
    mpz_clear(exposant);
    return "premier";
  }

  for(i=1 ; i<=k ; i++)
  {
    //détermination du nombre aléatoire
    mpz_urandomm(b, alea, max);
    mpz_add_ui(b, b, 2);

    expo_mod_rapide(b, exp_binaire, a, resultat, nombreBit);
    if(mpz_cmp_ui(resultat, 1) != 0)
    {
      mpz_clear(b);
      mpz_clear(max);
      mpz_clear(resultat);
      gmp_randclear(alea);
      mpz_clear(exposant);
      return "composé";
    }
  }

  mpz_clear(b);
  mpz_clear(max);
  mpz_clear(resultat);
  mpz_clear(exposant);
  gmp_randclear(alea);
  return "premier";
}

//décomposition
void decomposer(mpz_t n, mpz_t s, mpz_t t)
{
  mpz_t pariteT;
  mpz_init(pariteT);
  mpz_set(t, n);
  mpz_sub_ui(t, t, 1);
  mpz_mod_ui(pariteT, t, 2);

  while(mpz_cmp_ui(pariteT, 0) == 0)
  {
    mpz_div_ui(t, t, 2);
    mpz_add_ui(s, s, 1);
    mpz_mod_ui(pariteT, t, 2);
  }

  mpz_clear(pariteT);
}

//test de Miller-Rabin
char* test_Miller_Rabin(mpz_t n, int k)
{
  mpz_t pariteN, a, max, y, s, t, testY, j;
  int i, t_nombreBit, suivant_i;
  gmp_randstate_t(alea);
  mpz_init(pariteN);
  mpz_init(a);
  mpz_init(max);
  mpz_init(y);
  mpz_init(s);
  mpz_init(t);
  mpz_init(testY);
  mpz_init(j);
  gmp_randinit_default(alea);

  //deux valeurs automatiques
  if(mpz_cmp_ui(n, 2) == 0)
  {
    mpz_clear(pariteN);
    mpz_clear(a);
    mpz_clear(max);
    mpz_clear(y);
    mpz_clear(s);
    mpz_clear(t);
    mpz_clear(testY);
    mpz_clear(j);
    gmp_randclear(alea);
    return "premier";
  }
  if(mpz_cmp_ui(n, 1) == 0)
  {
    mpz_clear(pariteN);
    mpz_clear(a);
    mpz_clear(max);
    mpz_clear(y);
    mpz_clear(s);
    mpz_clear(t);
    mpz_clear(testY);
    mpz_clear(j);
    gmp_randclear(alea);
    return "c'est 1";
  }

  mpz_mod_ui(pariteN, n, 2);
  //si le nombre est pair alors il est composé
  if(mpz_cmp_ui(pariteN, 0) == 0)
  {
    mpz_clear(pariteN);
    mpz_clear(a);
    mpz_clear(max);
    mpz_clear(y);
    mpz_clear(s);
    mpz_clear(t);
    mpz_clear(testY);
    mpz_clear(j);
    gmp_randclear(alea);
    return "ce nombre est composé car il est pair";
  }

  mpz_set(max, n);
  mpz_sub_ui(max, max, 2);
  decomposer(n, s, t);

  //calcul de la représentation binaire de t
  t_nombreBit = mpz_sizeinbase(t, 2);
  int t_binaire[t_nombreBit];
  binaire(t, t_binaire, t_nombreBit);

  for(i=0; i < k; i++)
  {
    suivant_i = 0;

    //choix du nombre aléatoire
    mpz_urandomm(a, alea, max);
    mpz_add_ui(a, a, 1);

    expo_mod_rapide(a, t_binaire, n, y, t_nombreBit);


    mpz_mod(testY, y, n);
    if((mpz_cmp_ui(testY, 1) != 0) && (mpz_cmp_ui(testY, -1) != 0))
    {
      mpz_set_ui(j, 1);
      while(mpz_cmp(j, s) <= 0)
      {
        mpz_mul(y, y, y);
        mpz_mod(y, y, n);
        mpz_mod(testY, y, n);
        if(mpz_cmp_ui(testY, 1) == 0)
        {
          mpz_clear(pariteN);
          mpz_clear(a);
          mpz_clear(max);
          mpz_clear(y);
          mpz_clear(s);
          mpz_clear(t);
          mpz_clear(testY);
          mpz_clear(j);
          gmp_randclear(alea);
          return "premier";
        }
        if(mpz_cmp_si(testY, -1) == 0)
        {
          suivant_i = 1;
          break;
        }
        mpz_add_ui(j, j, 1);
      }
      if(suivant_i == 0)
      {
        mpz_clear(pariteN);
        mpz_clear(a);
        mpz_clear(max);
        mpz_clear(y);
        mpz_clear(s);
        mpz_clear(t);
        mpz_clear(testY);
        mpz_clear(j);
        gmp_randclear(alea);
        return "composé";
      }
    }
  }
  mpz_clear(pariteN);
  mpz_clear(a);
  mpz_clear(max);
  mpz_clear(y);
  mpz_clear(s);
  mpz_clear(t);
  mpz_clear(testY);
  mpz_clear(j);
  gmp_randclear(alea);
  return "premier";
}

int main(int argc, char const *argv[]) {
  mpz_t a, e, res, n;
  int k, choix;
  mpz_init(a);
  mpz_init(e);
  mpz_init(n);
  mpz_init(res);

  printf("Entrez un nombre à tester\n");
  gmp_scanf("%Zd", &a);

  printf("Entrez un nombre d'itération\n");
  gmp_scanf("%d", &k);

  printf("Tapez 1 pour le test Fermat ou tapez 2 pour le test de Miller-Rabin\n");
  scanf("%d", &choix);

  if(choix == 1)
  {
    printf("%s\n", test_fermat(a, k));
  }
  else if(choix == 2)
  {
    printf("%s\n", test_Miller_Rabin(a, k));
  }

  mpz_clear(a);
  mpz_clear(e);
  mpz_clear(res);
  mpz_clear(n);
  return 0;
}

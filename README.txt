Règles du Makefile :
  all -> exécute la règle crypto
  crypto -> compile le programme
  run -> compile et exécute le programme
  clear -> supprime l'exécutable

Fonctionnement global de l'application :

L'application demande à l'utilisateur d'entrer un nombre à tester et un nombre d'itérations.
Puis il choisit quel algorithme utiliser.

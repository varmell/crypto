all: crypto

crypto:
		gcc crypto.c -lgmp -o crypto

run: crypto
		./crypto

clear:
		rm crypto
